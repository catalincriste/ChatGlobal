using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
namespace ChatGlobal
{
    class AddTexto : AsyncTask
       
    {
        string user1;
        string texto1;
        string resp;

        Context context1;
        private ProgressDialog _progressDialog;
        public AddTexto(string user , string texto, Context context)
        {
             user1 = user;
            texto1 = texto;
            context1 = context;
        }
        protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
        {
            WebConnection wc = new WebConnection();
            resp = wc.sendPostRequest(MainActivity.URL_ADDTEXTO, user1, texto1);
            return resp;
        }

        protected override void OnPreExecute()
        {
            base.OnPreExecute();
           
        }

        protected override void OnPostExecute(Java.Lang.Object result)
        {
            base.OnPostExecute(result);
            _progressDialog.Dismiss();
            
            
        }
        public string getResp()
        {
            return resp;
        }
        public void setResp(string resp)
        {
            this.resp = resp;
        }
    }
}