﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace ChatGlobal
{
    [Activity(Label = "ChatGlobal", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
       // int count = 1;
        public static string URL_ADDTEXTO = "http://milinha.ddns.net/chatglobal/addTexto.php";
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            EditText user = FindViewById<EditText>(Resource.Id.user);
            EditText texto = FindViewById<EditText>(Resource.Id.texto);
            Button ok = FindViewById<Button>(Resource.Id.button1);
            TextView resposta = FindViewById<TextView>(Resource.Id.textViewResp);


            // Get our button from the layout resource,
            // and attach an event to it
           

            ok.Click += (object sender, EventArgs e) =>
            {
                AddTexto ad = new AddTexto(user.Text.ToString(),texto.Text.ToString(), this);
                ad.Execute();
                resposta.Text = ad.getResp();
            };
            // wc.sendPostRequest(URL_ADDTEXTO,);

        }
    }
    
    
}

